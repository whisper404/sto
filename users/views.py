import json
from helpers import AuthorRequiredMixin, get_page_list
from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth import get_user_model
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from django.views.generic.base import View
from .forms import ProfileForm, SignUpForm, UserLoginForm, ChangePwdForm, UploadImageForm
from .models import User
from django.db.models import Q
from django.template import RequestContext
from operation.models import UserCourse
from course.models import Course
from django.urls import reverse

# Create your views here.

User = get_user_model


class IndexView(View):
    def get(self, request):
        # all_banners = Banner.objects.all().order_by('index')
        courses = Course.objects.filter(is_banner=False)[:6]
        # banner_courses = Course.objects.filter(is_banner=True)[:3]
        return render(request, 'index.html', {
            # 'all_banners': all_banners,
            'courses': courses,
            # 'banner_courses': banner_courses,
        })


def login(request):
    nexturl = ""
    if request.method == 'POST':
        nexturl = request.POST.get('next', '/')
        form = UserLoginForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return redirect(nexturl)
        else:
            print(form.errors)
    else:
        nexturl = request.GET.get('next', '/')
        form = UserLoginForm()
    print(nexturl)
    return render(request, 'login.html', {'form': form, 'next': nexturl})


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password1 = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password1)
            auth_login(request, user)
            return redirect('index')
        else:
            print(form.errors)
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def logout(request):
    auth_logout(request)
    # return redirect('index')
    return HttpResponseRedirect(reverse('index'))


def change_password(request):
    if request.method == 'POST':
        form = ChangePwdForm(request.user, request.POST)
        if form.is_valid():
            if not user.is_staff and not user.is_superuser:
                user.save()
                update_session_auth_hash(request, user)
                messages.success(request, '修改成功')
                return redirect('users:change_password')
            else:
                messages.warning(request, '无权修改管理员密码')
                return redirect('users:change_password')
        else:
            form = ChangePwdForm(request.user)
        return render(request, 'change_password.html', {
            'form': form
        })


# class ProfileView(LoginRequiredMixin, AuthorRequiredMixin, generic.UpdateView):
#     model = User
#     form_class = ProfileForm
#     template_name = 'profile.html'

#     def get_success_url(self):
#         messages.success(self.request, "保存成功")
#         return reverse('users:profile', kwargs={'pk': self.request.user.pk})
class ProfileView(LoginRequiredMixin, View):

    def get(self, request):
        return render(request, 'usercenter-info.html', {})
        # return render(request, 'profile.html', {})

    def post(self, request):
        profile_form = ProfileForm(request.POST, instance=request.user)
        if profile_form.is_valid():
            profile_form.save()
            return HttpResponse('{"status": "保存成功"}',
                                content_type='application/json')
        else:
            return HttpResponse(json.dumps(profile_form.errors),
                                content_type='application/json')


class UploadImageView(LoginRequiredMixin, View):

    def post(self, request):
        image_form = UploadImageForm(request.POST, request.FILES)
        if image_form.is_valid():
            image = image_form.cleaned_data['image']
            request.user.image = image
            request.user.save()
            return HttpResponse('{status":"success"}',
            content_type='application/json')
        else:
            return HttpResponse('{"status":"fail"}',
            content_type='application/json')

class MyCourseView(LoginRequiredMixin, View):
    '''我的课程'''

    def get(self, request):
        user_courses = UserCourse.objects.filter(user=request.user)
        return render(request, "usercenter-mycourse.html", {
            "user_courses": user_courses,
        })


def page_not_found(request, exception=None):
    # 全局404处理函数
    response = render(request, '404.html', {})
    response.status_code = 404
    return response


def page_error(request, exception=None):
    # 全局500处理函数
    response = (request, '500.html', {})
    response.status_code = 500
    return response
