from django.urls import path
# from .views import UserinfoView, UploadImageView, UpdatePwdView, SendEmailCodeView
# from .views import UpdateEmailView, MyCourseView
from . import views
from .views import MyCourseView, UploadImageView

app_name = 'users'

urlpatterns = [
    path('login/', views.login, name='login'),
    path('signup/', views.signup, name='signup'),
    path('logout/', views.logout, name='logout'),
    # path('profile/<int:pk>/', views.ProfileView.as_view(), name='profile'),
    path('profile/', views.ProfileView.as_view(), name='profile'),
    path('image/avatar/', UploadImageView.as_view(), name='image_upload'),
    path('change_password/', views.change_password, name='change_password'),
    #我的课程
    path("mycourse/", MyCourseView.as_view(),name='mycourse'),
]
