from django.contrib.auth.models import AbstractUser
from django.db import models

from datetime import datetime


class User(AbstractUser):
    gender_choices = {
        ('M', '男'),
        ('F', '女')
    }
    nick_name = models.CharField('昵称', max_length=50, default='')
    birthday = models.DateField('生日', null=True, blank=True)
    gender = models.CharField(
        '性别', max_length=1, choices=gender_choices, default='')
    image = models.ImageField(upload_to='image/%Y%m',
                              default='image/default.png', max_length=100)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = '用户信息'