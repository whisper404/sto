from django.shortcuts import render
from django.views.generic import View
from .models import Course, CourseResource, Video
from operation.models import UserCourse
from pure_pagination import EmptyPage, PageNotAnInteger, Paginator
from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q


# Create your views here.


class CourseListView(View):
    def get(self, request):
        all_courses = Course.objects.all().order_by('-add_time')
        search_keywords = request.GET.get('keywords', '')
        if search_keywords:
            all_courses = all_courses.filter(
                Q(name__icontains=search_keywords) |
                Q(desc__icontains=search_keywords) |
                Q(detail__icontains=search_keywords))
        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1
        p = Paginator(all_courses, 2, request=request)
        courses = p.page(page)
        return render(request, 'course-list.html', {
            'all_courses': courses,
        })


class CourseDetailView(View):
    def get(self, request, course_id):
        course = Course.objects.get(id=int(course_id))
        # course.save()
        # user_courses = UserCourse.objects.filter(user=request.user, course=course)
        # if not user_courses:
        #     user_courses = UserCourse(user=request.user, course=course)
        #     user_courses.save()

        all_resources = CourseResource.objects.filter(course=course)
        return render(request, 'course-video.html', {
            'course': course,
            'all_resources': all_resources,
        })


class CourseInfoView(LoginRequiredMixin, View):
    def get(self, request, course_id):
        course = Course.objects.get(id=int(course_id))
        # course.save
        # user_courses = UserCourse.objects.filter(user=request.user, course=course)
        # if not user_courses:
        #     user_courses = UserCourse(user=request.user, course=course)
        #     user_courses.save()

        all_resources = CourseResource.objects.filter(course=course)
        return render(request, 'course-video.html', {
            'course': course,
            'all_resources': all_resources,
        })


class VideoPlayView(LoginRequiredMixin, View):
    def get(self, request, video_id):
        video = Video.objects.get(id=int(video_id))
        course = video.lesson.course
        # course.save()
        # user_courses = UserCourse.objects.filter(user=request.user, course=course)
        # if not user_courses:
        #     user_courses = UserCourse(user=request.user, course=course)
        #     user_courses.save()
        all_resources = CourseResource.objects.filter(course=course)
        return render(request, 'course-play.html', {
            'course': course,
            'all_resources': all_resources,
            'video': video,
        })
