from datetime import datetime

from django.db import models

# Create your models here.


class Course(models.Model):
    POSITION_CHOICES = (
        ("qtjd", "前台接待"),
        ("qtzw", "前台帐务"),
        ("dzcw", "店长财务")
    )
    name = models.CharField("课程名", max_length=50)
    desc = models.CharField("课程描述", max_length=300)
    detail = models.TextField("课程详情")
    position = models.CharField('职务', choices=POSITION_CHOICES, max_length=4)
    image = models.ImageField("封面图", upload_to="courses/%Y/%m", max_length=100)
    add_time = models.DateTimeField("添加时间", default=datetime.now)
    is_banner = models.BooleanField("是否轮播", default=False)

    class Meta:
        verbose_name = "课程"
        verbose_name_plural = verbose_name

    def get_zj_nums(self):
        return self.lesson_set.all().count()
    get_zj_nums.short_description = "章节数"

    def get_course_lesson(self):
        return self.lesson_set.all()

    def get_learn_users(self):
        return self.usercourse_set.all()[:5]

    def __str__(self):
        return self.name


class Lesson(models.Model):
    course = models.ForeignKey(
        Course, verbose_name='课程', on_delete=models.CASCADE)
    name = models.CharField("章节名", max_length=100)
    add_time = models.DateTimeField("添加时间", default=datetime.now)

    class Meta:
        verbose_name = "章节"
        verbose_name_plural = verbose_name

    def get_lesson_video(self):
        return self.video_set.all()

    def __str__(self):
        return '《{0}》课程的章节 >> {1}'.format(self.course, self.name)


class Video(models.Model):
    lesson = models.ForeignKey(
        Lesson, verbose_name="章节", on_delete=models.CASCADE)
    name = models.CharField("章节名", max_length=100)
    url = models.CharField('访问地址', default='/media/Video/', max_length=200)
    # file = models.FileField(
    #     "上传视频", upload_to="course/resource/%Y/%m", max_length=255)
    learn_times = models.IntegerField("学习时长(分钟数)", default=0)
    add_time = models.DateTimeField("添加时间", default=datetime.now)

    class Meta:
        verbose_name = "视频"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class CourseResource(models.Model):
    course = models.ForeignKey(
        Course, verbose_name="课程", on_delete=models.CASCADE)
    name = models.CharField("名称", max_length=100)
    download = models.FileField(
        "资源文件", upload_to="course/resource/%Y/%m", max_length=100)
    add_time = models.DateTimeField("添加时间", default=datetime.now)

    class Meta:
        verbose_name = "课程资源"
        verbose_name_plural = verbose_name
