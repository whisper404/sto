from django.contrib import admin
from django.urls import include, path, re_path
from django.views.generic import TemplateView
from django.views.static import serve
from studyonline.settings import MEDIA_ROOT
# from users.views import LoginView, RegisterView, ActiveUserView,ForgetPwdView, ResetView,ModifyPwdView
# from users.views import IndexView, login, signup, logout, change_password, ProfView
from users.views import IndexView
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='index'),
    # path('login/', login.as_view(), name='login'),
    # path('logout/', logout.as_view(), name='logout'),
    # path('signup/', signup.as_view(), name='signup'),
    # path('captcha/', include('captcha.urls')),
    # re_path('active/(?P<active_code>.*)/',
    #         ActiveUserView.as_view(), name='user_active'),
    # path('change_password/', change_password.as_view(), name='change_password'),
    # re_path('reset/(?P<active_code>.*)/',
    #         ResetView.as_view(), name='reset_pwd'),
    # path('modify_pwd/', ModifyPwdView.as_view(), name='modify_pwd'),
    re_path(r'^media/(?P<path>.*)', serve, {"document_root": MEDIA_ROOT}),

    # path("course/", include('course.urls', namespace="course")),
    # path("users/", include('users.urls', namespace="users")),
    path("course/", include('course.urls' )),
    path("users/", include('users.urls')),
]

handler404 = 'users.views.page_not_found'
handler500 = 'users.views.page_error'
